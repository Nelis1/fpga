module lcd12864(LCD_N, LCD_P, LCD_RST, PSB, clk, rs, rw, en, dat);
	output			LCD_N;
	output			LCD_P;
	output			LCD_RST;
	output			PSB;
	input				clk;
	output [7:0]	dat; 
	output reg		rs;
	output			rw, en;
	reg				e;
	reg [7:0]		dat;

	reg [15:0]		counter;
	reg [4:0]		current, next;
	reg				clkr;
	reg [1:0]		cnt;

	parameter	set0 = 6'h0;
	parameter	set1 = 6'h1;
	parameter	set2 = 6'h2;
	parameter	set3 = 6'h3;
	parameter	set4 = 6'h4;
	parameter	set_line_2 = 6'h5;
	parameter	set_line_3 = 6'h6;
	parameter	set_line_4 = 6'h7;

	parameter	dat0 = 6'h8;
	parameter	dat1 = 6'h9;
	parameter	dat2 = 6'hA;
	parameter	dat3 = 6'hB;
	parameter	dat4 = 6'hC;
	parameter	dat5 = 6'hD;
	parameter	dat6 = 6'hE;
	parameter	dat7 = 6'hF;
	parameter	dat8 = 6'h10;
	parameter	dat9 = 6'h11;
	parameter	dat10 = 6'h12;
	parameter	dat11 = 6'h13;
	parameter	dat12 = 6'h14;
	parameter	dat13 = 6'h15;
	parameter	dat14 = 6'h16;

	parameter	nul = 6'h17;
	
	assign LCD_N = 0;
	assign LCD_P = 1;
	assign en = clkr | e;
	assign rw = 0;
	assign LCD_RST = 1;
	assign PSB = 1;

	always @(posedge clk)
	begin
		counter = counter + 1;
		if (counter == 16'h000f)
			clkr = ~clkr;
	end

	always @(posedge clkr)
	begin
		current = next;
		case (current)
			set0:
			begin
				rs <= 0;			//0 to set configurations
				dat <= 8'h30;
				next <= set1;
			end
			set1:
			begin
				rs <= 0;
				dat <= 8'h0C;	//Display on Cursor off (0C)
				next <= set2;
			end
			set2:
			begin
				rs <= 0;
				dat <= 8'h6;	//Entry Mode (06)
				next <= set3;
			end
			set3:
			begin
				rs <= 0;
				dat <= 8'h1;	//Clear Display (01)
				next <= set4;
			end
			set4:
			begin
				rs <= 0;
				dat <= 8'h80;	//Force cursor to beginning of 1st line
				next <= dat0;
			end

			dat0:
			begin
				rs <= 1;
				dat <= "H";
				next <= dat1;
			end
			dat1:
			begin
				rs <= 1;
				dat <= "e";
				next <= dat2;
			end
			dat2:
			begin
				rs <= 1;
				dat <= "l";
				next <= dat3;
			end
			dat3:
			begin
				rs <= 1;
				dat <= "l";
				next <= dat4;
			end
			dat4:
			begin
				rs <= 1;
				dat <= "o";
				next <= dat5;
			end
			dat5:
			begin
				rs <= 1;
				dat <= " ";
				next <= dat6;
			end
			dat6:
			begin
				rs <= 1;
				dat <= "w";
				next <= dat7;
			end
			dat7:
			begin
				rs <= 1;
				dat <= "o";
				next <= dat8;
			end
			dat8:
			begin
				rs <= 1;
				dat <= "r";
				next <= dat9;
			end
			dat9:
			begin
				rs <= 1;
				dat <= "l";
				next <= dat10;
			end
			dat10:
			begin
				rs <= 1;
				dat <= "d";
				next <= dat11;
			end
			dat11:
			begin
				rs <= 1;
				dat <= "!";
				next <= set_line_2;
			end
			set_line_2:
			begin
				rs <= 0;			//0 to set configurations
				dat <= 8'h90;	//Force cursor to beginning of 2nd line
				next <= dat12;
			end
			dat12:
			begin
				rs <= 1;
				dat <= "1";
				next <= set_line_3;
			end
			set_line_3:
			begin
				rs <= 0;			//0 to set configurations
				dat <= 8'h88;	//Force cursor to beginning of 3rd line
				next <= dat13;
			end
			dat13:
			begin
				rs <= 1;
				dat <= "2";
				next <= set_line_4;
			end
			set_line_4:
			begin
				rs <= 0;			//0 to set configurations
				dat <= 8'h98;	//Force cursor to beginning of 4th line
				next <= dat14;
			end
			dat14:
			begin
				rs <= 1;
				dat <= "3";
				next <= nul;
			end

			nul:
			begin
				rs <= 0;
				dat <= 8'h00;
				if (cnt != 2'h2)
				begin
					e <= 0;
					next <= set0;
					cnt <= cnt + 1;
				end
				else
				begin
					next <= nul;
					e <= 1;
				end
			end
			default: next = set0;
		endcase
	end
endmodule
