module blinky (input clk, output reg led1, output reg led2);

	reg [31:0]	counter1;
	reg [31:0]	counter2;

	always @(posedge clk)
	begin
		if (counter1 <= 12500000)
		begin
			counter1 <= counter1 + 1;
		end

		else
		begin
			counter1 <= 0;
			led1 <= ~led1;
		end

		if (counter2 <= 25000000)
		begin
			counter2 <= counter2 + 1;
		end

		else
		begin
			counter2 <= 0;
			led2 <= ~led2;
		end
	end
endmodule
