
State Machine - |lcd1602_btn|next
Name next.set3 next.set2 next.set1 next.set0 next.set4 
next.set0 0 0 0 0 0 
next.set1 0 0 1 1 0 
next.set2 0 1 0 1 0 
next.set3 1 0 0 1 0 
next.set4 0 0 0 1 1 

State Machine - |lcd1602_btn|current
Name current.set3 current.set2 current.set1 current.set0 current.set4 
current.set0 0 0 0 0 0 
current.set1 0 0 1 1 0 
current.set2 0 1 0 1 0 
current.set3 1 0 0 1 0 
current.set4 0 0 0 1 1 
