module lcd1602(clk, rs, rw, en, dat, LCD_N, LCD_P);
	input				clk;
	output [7:0]	dat;
	output			rs, rw, en, LCD_N, LCD_P;

	reg			e; 
	reg [7:0]	dat;
	reg			rs;
	reg [15:0]	counter;
	reg [4:0]	current, next;
	reg			clkr;
	reg [1:0]	cnt;

	parameter	set0 = 4'h0;
	parameter	set1 = 4'h1;
	parameter	set2 = 4'h2;
	parameter	set3 = 4'h3;
	parameter	set4 = 4'h4;
	parameter	set_line_2 = 4'h5;
	parameter	dat0 = 4'h6;
	parameter	dat1 = 4'h7;
	parameter	dat2 = 4'h8;
	parameter	dat3 = 4'h9;
	parameter	dat4 = 4'hA;
	parameter	dat5 = 4'hB;

	parameter	dat6 = 4'hC;
	parameter	dat7 = 4'hD;
	parameter	dat8 = 4'hE;
	parameter	dat9 = 4'hF;
	parameter	dat10 = 5'h10;
	parameter	dat11 = 5'h11;
	parameter	dat12 = 5'h12;
	parameter	dat13 = 5'h13;
	parameter	dat14 = 5'h14;
	parameter	dat15 = 5'h15;
	parameter	dat16 = 5'h16;
	parameter	dat17 = 5'h17;
	parameter	dat18 = 5'h18;
	parameter	dat19 = 5'h19;
	parameter	dat20 = 5'h1A;
	parameter	dat21 = 5'h1B;
	parameter	dat22 = 5'h1C;
	parameter	nul = 5'h1D;

	assign LCD_N = 0;
	assign LCD_P = 1;

	always @(posedge clk)
	begin
		counter = counter + 1;
		if (counter == 16'h000f)
			clkr = ~clkr;
	end

	always @(posedge clkr)
	begin
		current = next;
		case (current)
			set0:
			begin
				rs <= 0;			//0 to set configurations
				dat <= 8'h38;	//set lines (38->2 lines | 31->1 line(need to ajust pwm))
				next <= set1;
			end
			set1:
			begin
				rs <= 0;			//0 to set configurations
				dat <= 8'h0C;	//Display on Cursor off (0C)
				next <= set2;
			end
			set2:
			begin
				rs <= 0;			//0 to set configurations
				dat <= 8'h6;	//Entry Mode (06)
				next <= set3;
			end
			set3:
			begin
				rs <= 0;			//0 to set configurations
				dat <= 4'h1;	//Clear Display (01)
				next <= set4;
			end
			set4:
			begin
				rs <= 0;			//0 to set configurations
				dat <= 8'h80;	//Force cursor to beginning of 1st line
				next <= dat0;
			end

			dat0:
			begin
				rs <= 1;			//1 to give characters
				dat <= "H";
				next <= dat1;
			end
			dat1:
			begin
				rs <= 1;
				dat <= "e";
				next <= dat2;
			end
			dat2:
			begin
				rs <= 1;
				dat <= "l";
				next <= dat3;
			end
			dat3:
			begin
				rs <= 1;
				dat <= "l";
				next <= dat4;
			end
			dat4:
			begin
				rs <= 1;
				dat <= "o";
				next <= dat5;
			end
			dat5:
			begin
				rs <= 1;
				dat <= " ";
				next <= dat6;
			end
			dat6:
			begin
				rs <= 1;
				dat <= "W";
				next <= dat7;
			end
			dat7:
			begin
				rs <= 1;
				dat <= "o";
				next <= dat8;
			end
			dat8:
			begin
				rs <= 1;
				dat <= "r";
				next <= dat9;
			end
			dat9:
			begin
				rs <= 1;
				dat <= "l";
				next <= dat10;
			end
			dat10:
			begin
				rs <= 1;
				dat <= "d";
				next <= dat11;
			end
			dat11:
			begin
				rs <= 1;
				dat <= "!";
				next <= set_line_2;
			end
			set_line_2:
			begin
				rs <= 0;			//0 to set configurations
				dat <= 8'hC0;	//Force cursor to beginning of 2nd line
				next <= dat12;
			end
			dat12:
			begin
				rs <= 1;
				dat <= "T";
				next <= dat13;
			end
			dat13:
			begin
				rs <= 1;
				dat <= "h";
				next <= dat14;
			end
			dat14:
			begin
				rs <= 1;
				dat <= "i";
				next <= dat15;
			end
			dat15:
			begin
				rs <= 1;
				dat <= "s";
				next <= dat16;
			end
			dat16:
			begin
				rs <= 1;
				dat <= " ";
				next <= dat17;
			end
			dat17:
			begin
				rs <= 1;
				dat <= "w";
				next <= dat18;
			end
			dat18:
			begin
				rs <= 1;
				dat <= "o";
				next <= dat19;
			end
			dat19:
			begin
				rs <= 1;
				dat <= "r";
				next <= dat20;
			end
			dat20:
			begin
				rs <= 1;
				dat <= "k";
				next <= dat21;
			end
			dat21:
			begin
				rs <= 1;
				dat <= "s";
				next <= dat22;
			end
			dat22:
			begin
				rs <= 1;
				dat <= "!";
				next <= nul;
			end

			nul:
			begin
				rs <= 0;
				dat <= 8'h00;
				if (cnt != 2'h2)
				begin
					e <= 0;
					next <= set0;
					cnt <= cnt + 1;
				end
				else
				begin
					next <= nul;
					e <= 1;
				end
			end
			default:
				next = set0;
		endcase
	end

	assign en = clkr | e;
	assign rw = 0;
endmodule
