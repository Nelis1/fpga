//clear screen. when joystick z is pressed then default char is printed. Change letter with joystick up or down.
//Reset also clear screen.

module lcd1602_btn(clk, reset, rs, rw, en, dat, LCD_N, LCD_P, key_z, key_up, key_down);
	input	clk;
	input	key_z;
	input	key_up;
	input	key_down;
	input	reset;

	output [7:0]	dat;
	output			rs, rw, en, LCD_N, LCD_P;

	reg			e; 
	reg [7:0]	dat;
	reg [7:0]	letter;
	reg			rs;
	reg [31:0]	counter;
	reg [4:0]	current, next;
	reg			clkr;
	reg [3:0]	setup;

	parameter	set0 = 4'h0;
	parameter	set1 = 4'h1;
	parameter	set2 = 4'h2;
	parameter	set3 = 4'h3;
	parameter	set4 = 4'h4;
	parameter	dat0 = 4'h5;
	parameter	nul = 5'h6;

	assign	LCD_N = 0;
	assign	LCD_P = 1;

	always @(posedge clk or negedge reset)
	if (!reset)
	begin
		setup <= 0;
		if (counter <= 25000000)
		begin
			counter <= counter + 1;
		end
		else
		begin
			counter <= 0;
			clkr <= ~clkr;
		end
	end
	else
	begin
		if (!key_up)
			letter <= "d";
		else if (!key_down)
			letter <= "s";
		if (counter <= 250000)
		begin
			counter <= counter + 1;
		end
		else if (counter >= 250000)
		begin
			counter <= 0;
			if (setup <= 12)
			begin
				setup <= setup + 1;
				clkr <= ~clkr;
			end
			else
			begin
				if (!key_z)
					clkr <= 1'b1;
				else
					clkr <= 1'b0;
			end
		end
	end

	always @(posedge clkr or negedge reset)
	if (!reset)
	begin
		next <= set0;
		rs <= 0;			//0 to set configurations
		dat <= 4'h1;	//Clear Display (01)
	end
	else
	begin
		if (key_z)
		begin
			current = next;
			case (current)
				set0:
				begin
					rs <= 0;			//0 to set configurations
					dat <= 8'h38;	//set lines (38->2 lines | 31->1 line(need to ajust pwm))
					next <= set1;
				end
				set1:
				begin
					rs <= 0;			//0 to set configurations
					dat <= 8'h0C;	//Display on Cursor off (0C)
					next <= set2;
				end
				set2:
				begin
					rs <= 0;			//0 to set configurations
					dat <= 8'h6;	//Entry Mode (06)
					next <= set3;
				end
				set3:
				begin
					rs <= 0;			//0 to set configurations
					dat <= 4'h1;	//Clear Display (01)
					next <= set4;
				end
				set4:
				begin
					rs <= 0;			//0 to set configurations
					dat <= 8'h80;	//Force cursor to beginning of 1st line
//					next <= dat0;
				end

//				dat0:
//				begin
//					rs <= 1;			//1 to give characters
//					dat <= "H";
//					next <= nul;
//				end

				default:
					next = set0;
			endcase
		end
		else
		begin
			if (!key_z)
			begin
				rs <= 1;			//1 to give characters
				dat <= letter;
			end
		end
	end

	assign en = clkr | e;
	assign rw = 0;
endmodule
