module joystick(clk, reset, key_up, key_down, key_left, key_right, key_z, led);
	input				clk, reset;
	input				key_up, key_down, key_left, key_right, key_z;
	output [3:0]	led;

	reg [3:0]		led_reg;

	always @(posedge clk or negedge reset)
	if (!reset)
		led_reg <= 4'b1111;
	else if (key_z == 1'b0)
		led_reg <= 4'b0000;
	else if (key_up == 1'b0)
		led_reg <= 4'b1110;
	else if (key_down == 1'b0)
		led_reg <= 4'b0111;
	else if (key_left == 1'b0)
		led_reg <= 4'b1101;
	else if (key_right == 1'b0)
		led_reg <= 4'b1011;

	assign led = led_reg;
endmodule
